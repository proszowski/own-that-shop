#Own That Shop

##Opis

Jest to prosty program (adminka) do obsługi sklepu. Została również zaimplementowana bardzo uproszczona wersja sklepu z możliwością rejestracji użytkownika, logowania się, z podziałem na role oraz z możliwością zakupu przedmiotu, jednak była to część poboczna. 

###Admin
Poniższy screen menu dobrze wyjaśnia przeznaczenie adminki:

![Alt text](/screenshots/menu.png?raw=true "Menu")

Jesteśmy w stanie zarządzać klientami, produktami oraz transakcjami. Mamy również zakładkę statystyki, w której jesteśmy w stanie zobaczyć dane przefiltrowane w różny sposób 

###Klienci

![Alt text](/screenshots/clients.png?raw=true "Klienci")
- możliwość dodania nowego klienta  
- możliwość usunięcia klienta  
- możliwość modyfikacji klienta  
- możliwość przeglądania danych klientów  
###Produkty
Analogicznie jak dla klientów, CRUD dla produktów:
![Alt text](/screenshots/products.png?raw=true "Produkty")

###Transakcje
![Alt text](/screenshots/transactions.png?raw=true "Statystyki")
- możliwość zobaczenia transakcji i jej szczegółów  
- możliwość dodania transakcji  


###Statystyki
![Alt text](/screenshots/statistics.png?raw=true "Statystyki")
- możliwość przeglądania danych z użycie różnych filtrów
Dostępne filtry:
	- cena produktu (min/max)  
	- data zakupu  
	- dla konkretnego klienta  
	- typ klienta  
 	- dla konkretnego produktu  

###Sklep
![Alt text](/screenshots/shop.png?raw=true "Sklep")

###Historia zakupów
![Alt text](/screenshots/history.png?raw=true "Historia")

##Architektura
REST API - po stronie którego jest cała logika aplikacji (np. wydobywanie danych do statystyk czy walidacja encji) serwuje dane klientom. Główne założenie jest takie, że po stronie klienta dane są tylko i wyłącznie wyświetlane. [Link do repozytoria front-endu](https://bitbucket.org/proszowski/own-that-shop-front-end/src/master/) 

###Schemat bazy danych:
![Alt text](/screenshots/schema.png?raw=true "schema")

###Wystawione endpointy:
![Alt text](/screenshots/endpoints.png?raw=true "schema")
+ endpointy actuatora.

##Technologie
Do backendu użyłem Javy ze Spring-Bootem. Jako bazę danych wykorzystałem PostgreSQL w Dockerowym kontenerze.  

Za narzędzie do tworzenia front-endu posłużył mi Angular.  

##Instrukcja
1. Baza danych:  
a) docker run --rm   --name own-that-shop -e POSTGRES_PASSWORD=docker -d -p 5430:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres  
b) psql -U postgres -h localhost -p 5430  
c) create database ownthatshop; \q   
2. Backend
a) będąc w folderze z projektem (tam gdzie plik pom.xml) uruchamiamy:  
mvn spring-boot:run  
3. Frontend 
a) będąc w folderze z projektem (tam gdzie package.json) uruchamiamy komendę:   
ng serve -o  
