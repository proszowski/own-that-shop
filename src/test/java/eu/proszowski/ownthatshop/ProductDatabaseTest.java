package eu.proszowski.ownthatshop;

import eu.proszowski.ownthatshop.model.product.ProductFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductDatabaseTest {

    @Autowired
    private ProductFacade productFacade;

    @Test
    public void should_return_all_products(){
        this.productFacade.getAll();
        //check if exception is thrown or not
    }
}
