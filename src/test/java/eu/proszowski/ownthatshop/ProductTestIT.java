package eu.proszowski.ownthatshop;

import eu.proszowski.ownthatshop.model.product.ProductDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductTestIT {
    TestRestTemplate restTemplate = new TestRestTemplate();
    @LocalServerPort
    private int port;

    @Test
    public void should_return_product() {
        //given
        ProductDto productDto = getValidProductDto();
        ResponseEntity<Long> response = restTemplate.postForEntity(getBaseUrl() + "/add", productDto, Long.class);
        //when
        ResponseEntity<ProductDto> responseProduct = restTemplate.getForEntity(getBaseUrl() + "/" + response.getBody(), ProductDto.class);
        //then
        assertSame(HttpStatus.OK, responseProduct.getStatusCode());
        assertEquals(responseProduct.getBody().getName(), productDto.getName());
    }

    @Test
    public void should_add_valid_product() {
        //given
        ProductDto productDto = getValidProductDto();
        //when
        ResponseEntity<String> response = restTemplate.postForEntity(getBaseUrl() + "/add", productDto, String.class);
        //then
        assertSame(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void should_return_error_code_when_adding_invalid_product() {
        //given
        String shorterThanFiveCharacters = "1234";
        ProductDto productDto = getProductDtoWithName(shorterThanFiveCharacters);
        //when
        ResponseEntity<String> response = restTemplate.postForEntity(getBaseUrl() + "/add", productDto, String.class);
        //then
        assertSame(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void should_throw_bad_request_when_try_to_update_non_existing_product() throws URISyntaxException {
        //given
        ProductDto productDto = new ProductDto(
                123L,
                "valid name",
                BigDecimal.valueOf(10),
                10,
                "",
                1L
        );

        RequestEntity<ProductDto> requestEntity = RequestEntity.put(URI.create(getBaseUrl() + "/update")).body(productDto);
        //when
        ResponseEntity<Long> response = restTemplate.exchange(requestEntity, Long.class);

    }

    @Test
    public void should_update_product_properly() {
        //given
        ProductDto productDto = new ProductDto(
                null,
                "valid name",
                BigDecimal.valueOf(10),
                10,
                "",
                1L
        );
        ResponseEntity<Long> response = restTemplate.postForEntity(getBaseUrl() + "/add", productDto, Long.class);
        Long productId = response.getBody();
        BigDecimal newPrice = BigDecimal.valueOf(100.00);
        //when
        productDto.setId(productId);
        productDto.setPrice(newPrice);
        restTemplate.put(getBaseUrl() + "/update", productDto, Long.class);
        //then
        ResponseEntity<ProductDto> responseEntity = restTemplate.getForEntity(getBaseUrl() + "/" + productId, ProductDto.class);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertEquals(newPrice.stripTrailingZeros(), responseEntity.getBody().getPrice().stripTrailingZeros());
        assertEquals(productId, responseEntity.getBody().getId());
    }

    @Test
    public void should_remove_product(){
        //given
        ProductDto productDto = getValidProductDto();
        ResponseEntity<Long> responseId = restTemplate.postForEntity(getBaseUrl() + "/add", productDto, Long.class);
        Long productId = responseId.getBody();
        //when
        RequestEntity requestEntity = new RequestEntity(HttpMethod.DELETE, URI.create(getBaseUrl() + "/delete/" + productId));
        ResponseEntity<ProductDto> deleteResponse = restTemplate.exchange(requestEntity, ProductDto.class);
        //then
        assertSame(HttpStatus.OK, deleteResponse.getStatusCode());
    }

    @Test
    public void should_throw_exception_when_product_to_remove_does_not_exist(){
        //given
        Long dummyId = 9929L;
        //when
        RequestEntity requestEntity = new RequestEntity(HttpMethod.DELETE, URI.create(getBaseUrl() + "/delete/" + dummyId));
        ResponseEntity<ProductDto> deleteResponse = restTemplate.exchange(requestEntity, ProductDto.class);
        //then
        assertSame(HttpStatus.NOT_FOUND, deleteResponse.getStatusCode());
    }

    private ProductDto getValidProductDto() {
        return getProductDtoWithName("valid name");
    }

    private ProductDto getProductDtoWithName(String shorterThanFiveCharacters) {
        return new ProductDto(
                null,
                shorterThanFiveCharacters,
                new BigDecimal("99.99"),
                10,
                "",
                1L
        );
    }

    private String getBaseUrl() {
        return "http://localhost:" + port + "/api/admin/product";
    }
}
