package eu.proszowski.ownthatshop.controllers;

import eu.proszowski.ownthatshop.model.ModelException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ExceptionHandlerAdvice {
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> exceptionHandler(ConstraintViolationException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ModelException.class)
    public ResponseEntity<String> modelExceptionHandler(ModelException e) {
        if(e.getMessage().toLowerCase().contains("not found")){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
