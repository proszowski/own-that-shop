package eu.proszowski.ownthatshop.controllers;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.product.ProductFacade;
import eu.proszowski.ownthatshop.model.product.ProductNotFoundException;
import eu.proszowski.ownthatshop.model.transaction.TransactionDto;
import eu.proszowski.ownthatshop.model.transaction.TransactionFacade;
import eu.proszowski.ownthatshop.model.transaction.TransactionMapper;
import eu.proszowski.ownthatshop.service.AvailabilityChecker;
import eu.proszowski.ownthatshop.service.AvailabilityStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/admin/transaction")
@CrossOrigin("http://localhost:4200")
public class TransactionController {

    private TransactionFacade transactionFacade;
    private ProductFacade productFacade;
    private AvailabilityChecker availabilityChecker;
    private TransactionMapper transactionMapper;

    public TransactionController(TransactionFacade transactionFacade,
            ProductFacade productFacade,
            AvailabilityChecker availabilityChecker,
            TransactionMapper transactionMapper) {
        this.transactionFacade = transactionFacade;
        this.productFacade = productFacade;
        this.availabilityChecker = availabilityChecker;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping
    public Collection<TransactionDto> getAll() {
        return transactionFacade.getAll().stream().map(transaction -> transactionMapper.toDto(transaction)).collect(
                Collectors.toList());
    }

    @GetMapping("/{id}")
    public TransactionDto getById(@PathVariable Long id) {
        return transactionMapper.toDto(transactionFacade.getById(id));
    }

    @PostMapping("/add")
    public Long add(@RequestBody TransactionDto transactionDto) {
        return transactionFacade.add(transactionMapper.fromDto(transactionDto));
    }

    @DeleteMapping("/delete/{id}")
    public void remove(@PathVariable Long id) {
        transactionFacade.remove(id);
    }

    @PostMapping("check-availability/{productId}")
    public AvailabilityStatus checkAvailability(@RequestBody TransactionDto transactionDto,
            @PathVariable Long productId) {
        System.out.println(transactionDto);
        Optional<Product> product = productFacade.get(productId);
        if (product.isPresent()) {
            return availabilityChecker.getAvailabilityStatus(transactionDto, product.get());
        } else {
            throw new ProductNotFoundException();
        }
    }
}
