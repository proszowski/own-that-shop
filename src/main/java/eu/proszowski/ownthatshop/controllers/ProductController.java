package eu.proszowski.ownthatshop.controllers;

import eu.proszowski.ownthatshop.model.product.*;
import eu.proszowski.ownthatshop.model.transaction.TransactionFacade;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("api/product")
@CrossOrigin(value = "http://localhost:4200")
public class ProductController {

    private ProductFacade productFacade;
    private ProductMapper productMapper;

    public ProductController(ProductFacade productFacade, ProductMapper productMapper) {
        this.productFacade = productFacade;
        this.productMapper = productMapper;
    }

    @GetMapping
    public Collection<Product> getAll() {
        return productFacade.getAll();
    }

    @GetMapping("{id}")
    public ProductDto get(@PathVariable Long id) {
        Optional<Product> productOptional = productFacade.get(id);
        if(productOptional.isPresent()){
            return productMapper.toDto(productOptional.get());
        }else {
            throw new ProductNotFoundException();
        }
    }

    @PostMapping("add")
    public Long add(@RequestBody @Valid Product product) {
        return productFacade.add(product);
    }

    @PutMapping("update")
    public void update(@RequestBody @Valid Product product) {
        productFacade.update(product);
    }

    @GetMapping("available-types")
    public Collection<ProductType> getAvailableTypes(){
        return productFacade.getAvailableTypes();
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        productFacade.remove(id);
    }
}

