package eu.proszowski.ownthatshop.controllers;

import eu.proszowski.ownthatshop.model.ModelException;
import eu.proszowski.ownthatshop.model.client.*;
import eu.proszowski.ownthatshop.model.transaction.TransactionFacade;
import eu.proszowski.ownthatshop.security.model.User;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("/api/admin/client")
public class ClientController {

    private ClientFacade clientFacade;
    private TransactionFacade transactionFacade;

    public ClientController(ClientFacade clientFacade,
            TransactionFacade transactionFacade) {
        this.clientFacade = clientFacade;
        this.transactionFacade = transactionFacade;
    }

    @GetMapping()
    public Collection<Client> getAll() {
        return clientFacade.getAll();
    }

    @GetMapping("/{id}")
    public Client getById(@PathVariable Long id) {
        return clientFacade.getById(id);
    }

    @GetMapping("/types")
    public Collection<ClientType> getTypes() {
        return clientFacade.getAllClientTypes();
    }

    @GetMapping("/companies")
    public Collection<Company> getCompanies() {
        return clientFacade.getAllCompanies();
    }


    @PostMapping("/add")
    public Long add(@RequestBody Client client) {
        return clientFacade.add(client);
    }

    @PutMapping("/update")
    public void update(@RequestBody @Valid Client client) {
        clientFacade.update(client);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        transactionFacade.removeAllWithClientId(id);
        clientFacade.remove(id);
    }

}
