package eu.proszowski.ownthatshop.controllers;

import eu.proszowski.ownthatshop.model.client.Client;
import eu.proszowski.ownthatshop.model.client.ClientFacade;
import eu.proszowski.ownthatshop.model.transaction.TransactionDetailsDto;
import eu.proszowski.ownthatshop.model.transaction.TransactionDto;
import eu.proszowski.ownthatshop.model.transaction.TransactionFacade;
import eu.proszowski.ownthatshop.model.transaction.TransactionMapper;
import eu.proszowski.ownthatshop.security.services.UserPrinciple;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/api/shop/")
public class ShopController {

    private ClientFacade clientFacade;
    private TransactionFacade transactionFacade;
    private TransactionMapper transactionMapper;

    public ShopController(ClientFacade clientFacade,
            TransactionFacade transactionFacade,
            TransactionMapper transactionMapper) {
        this.clientFacade = clientFacade;
        this.transactionFacade = transactionFacade;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping("whoami")
    public Client whoAmI() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return clientFacade.getByEmail(userPrinciple.getEmail());
    }

    @GetMapping("history")
    public List<TransactionDto> myTransactions() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Client client = clientFacade.getByEmail(userPrinciple.getEmail());

        return transactionFacade.getAllWithClientId(client.getId()).stream()
                .map(transaction -> transactionMapper.toDto(transaction)).collect(
                        Collectors.toList());
    }

    @GetMapping("buy/{productId}")
    public void buyItem(@PathVariable Long productId) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Client client = clientFacade.getByEmail(userPrinciple.getEmail());
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setClientId(client.getId());
        TransactionDetailsDto transactionDetailsDto = new TransactionDetailsDto();
        transactionDetailsDto.setProductId(productId);
        transactionDetailsDto.setQuantity(1);
        transactionDto.setTransactionDetails(Collections.singleton(transactionDetailsDto));
        transactionFacade.add(transactionMapper.fromDto(transactionDto));
    }
}
