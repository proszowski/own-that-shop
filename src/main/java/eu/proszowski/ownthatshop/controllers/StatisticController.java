package eu.proszowski.ownthatshop.controllers;

import eu.proszowski.ownthatshop.model.statistic.StatisticFacade;
import eu.proszowski.ownthatshop.model.statistic.query.QueryBinder;
import eu.proszowski.ownthatshop.model.statistic.query.QueryDto;
import eu.proszowski.ownthatshop.model.statistic.ResultType;
import eu.proszowski.ownthatshop.model.statistic.Statistic;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/statistic")
@CrossOrigin(value = "http://localhost:4200")
public class StatisticController {

    private StatisticFacade statisticFacade;

    public StatisticController(StatisticFacade statisticFacade) {
        this.statisticFacade = statisticFacade;
    }

    @PostMapping("{resultType}")
    public Statistic getStatistics(@RequestBody QueryDto queryDto, @PathVariable ResultType resultType){
        return statisticFacade.getStatistics(QueryBinder.bind(queryDto), resultType);
    }
}
