package eu.proszowski.ownthatshop.service;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.transaction.TransactionDto;
import lombok.val;

public class AvailabilityChecker {

    public AvailabilityStatus getAvailabilityStatus(TransactionDto transaction, Product product) {
        if(product == null || transaction == null){
            return AvailabilityStatus.notAvailable(0);
        }

        Integer totalQuantity = 0;
        for(val transactionDetails : transaction.getTransactionDetails()){
            if(transactionDetails.getProductId().equals(product.getId())){
                totalQuantity += transactionDetails.getQuantity();
            }
        }

        int difference = product.getQuantity() - totalQuantity;
        if(difference < 0){
            return AvailabilityStatus.notAvailable(product.getQuantity());
        }

        return AvailabilityStatus.available(product.getQuantity());
    }
}
