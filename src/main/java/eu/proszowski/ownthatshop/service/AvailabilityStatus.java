package eu.proszowski.ownthatshop.service;

public class AvailabilityStatus {
    public Boolean isAvailable;
    public Integer actualQuantity;

    private AvailabilityStatus(Boolean isAvailable, Integer actualQuantity) {
        this.isAvailable = isAvailable;
        this.actualQuantity = actualQuantity;
    }

    public static AvailabilityStatus notAvailable(int quantity) {
        return new AvailabilityStatus(false, quantity);
    }

    public static AvailabilityStatus available(Integer quantity) {
        return new AvailabilityStatus(true, quantity);
    }
}
