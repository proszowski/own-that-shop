package eu.proszowski.ownthatshop;

import eu.proszowski.ownthatshop.database.client.*;
import eu.proszowski.ownthatshop.database.product.JpaProductRepository;
import eu.proszowski.ownthatshop.database.product.JpaProductTypeRepository;
import eu.proszowski.ownthatshop.database.product.ProductDatabase;
import eu.proszowski.ownthatshop.database.transaction.JpaTransactionRepository;
import eu.proszowski.ownthatshop.database.transaction.TransactionDatabase;
import eu.proszowski.ownthatshop.model.client.Client;
import eu.proszowski.ownthatshop.model.client.ClientFacade;
import eu.proszowski.ownthatshop.model.client.ClientMapper;
import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.product.ProductFacade;
import eu.proszowski.ownthatshop.model.product.ProductMapper;
import eu.proszowski.ownthatshop.model.statistic.StatisticFacade;
import eu.proszowski.ownthatshop.model.transaction.*;
import eu.proszowski.ownthatshop.service.AvailabilityChecker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;
import java.util.List;

@Configuration
class AppConfiguration {

    @Bean
    ClientMapper clientMapper(ClientFacade clientFacade) {
        return new ClientMapper(clientFacade);
    }

    @Bean
    ProductMapper productMapper(ProductFacade productFacade) {
        return new ProductMapper(productFacade);
    }

    @Bean
    ProductFacade productFacade(JpaProductRepository productRepository,
            JpaProductTypeRepository productTypeRepository) {
        ProductDatabase productDatabase = new ProductDatabase(productRepository, productTypeRepository);
        return new ProductFacade(productDatabase);
    }

    @Bean
    ClientFacade clientFacade(JpaClientRepository jpaClientRepository, JpaClientTypeRepository jpaClientTypeRepository,
            JpaContactRepository jpaContactRepository, JpaAddressRepository jpaAddressRepository,
            JpaCompanyRepository jpaCompanyRepository) {
        ClientDatabase clientDatabase = new ClientDatabase(jpaClientRepository, jpaClientTypeRepository,
                                                           jpaContactRepository, jpaAddressRepository,
                                                           jpaCompanyRepository);
        return new ClientFacade(clientDatabase);
    }

    @Bean
    TransactionFacade transactionFacade(JpaTransactionRepository jpaTransactionRepository){
        TransactionRepository transactionRepository = new TransactionDatabase(jpaTransactionRepository);
        return new TransactionFacade(transactionRepository);
    }

    @Bean
    AvailabilityChecker availabilityChecker(){
        return new AvailabilityChecker();
    }

    @Bean
    TransactionMapper transactionMapper(ClientFacade clientFacade, TransactionDetailsMapper transactionDetailsMapper){
        return new TransactionMapper(clientFacade, transactionDetailsMapper);
    }

    @Bean
    TransactionDetailsMapper transactionDetailsMapper(ProductFacade productFacade){
        return new TransactionDetailsMapper(productFacade);
    }

    @Bean
    StatisticFacade statisticFacade(ClientFacade clientFacade, ProductFacade productFacade, TransactionFacade transactionFacade){
        return new StatisticFacade(clientFacade, productFacade, transactionFacade);
    }
}
