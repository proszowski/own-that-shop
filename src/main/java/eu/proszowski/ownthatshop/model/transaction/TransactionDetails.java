package eu.proszowski.ownthatshop.model.transaction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import eu.proszowski.ownthatshop.model.product.Product;
import lombok.Data;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "TransactionDetails")
public class TransactionDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @OneToOne
    @JoinColumn(name = "product_id", nullable = false)
    @Valid
    private Product product;
    @Positive
    private Integer quantity;
    @Digits(integer = 6, fraction = 2)
    @Positive
    private BigDecimal totalPrice;
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private Transaction transaction;
}
