package eu.proszowski.ownthatshop.model.transaction;

import eu.proszowski.ownthatshop.model.client.exception.ClientNotFoundException;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TransactionFacade {

    private TransactionRepository transactionRepository;

    public TransactionFacade(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Collection<Transaction> getAll() {
        List<Transaction> transactions = new LinkedList<>();
        transactionRepository.findAll().forEach(transactions::add);
        return transactions;
    }

    public Long add(Transaction transaction) {
        transaction.setId(null);
        return transactionRepository.add(transaction);
    }

    public void remove(Long id) {
        transactionRepository.deleteById(id);
    }

    public Transaction getById(Long id) {
        Optional<Transaction> optional = transactionRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new TransactionNotFoundException();
        }
    }

    public void removeAllWithClientId(Long id) {
        List<Long> idsToRemove = new LinkedList<>();
        transactionRepository.findAll().forEach(
                transaction -> {
                    if (transaction.getClient().getId().equals(id)) {
                        idsToRemove.add(transaction.getId());
                    }
                }
        );

        idsToRemove.forEach(transactionRepository::deleteById);
    }

    public List<Transaction> getAllWithClientId(Long id) {
        Optional<List<Transaction>> optional = transactionRepository.findByClientId(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ClientNotFoundException();
        }
    }
}
