package eu.proszowski.ownthatshop.model.transaction;

import eu.proszowski.ownthatshop.model.client.Client;
import eu.proszowski.ownthatshop.model.client.ClientFacade;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionMapper {

    private ClientFacade clientFacade;
    private TransactionDetailsMapper transactionDetailsMapper;

    public TransactionMapper(ClientFacade clientFacade,
            TransactionDetailsMapper transactionDetailsMapper) {
        this.clientFacade = clientFacade;
        this.transactionDetailsMapper = transactionDetailsMapper;
    }

    public Transaction fromDto(TransactionDto transactionDto) {
        System.out.println(transactionDto);
        Transaction transaction = new Transaction();
        transaction.setDate(Instant.now());
        Client client = clientFacade.getById(transactionDto.getClientId());
        transaction.setClient(client);
        List<TransactionDetails> transactionDetails = new LinkedList<>();
        transactionDto.getTransactionDetails().forEach(
                td -> transactionDetails.add(transactionDetailsMapper.fromDto(td, transaction))
        );
        transaction.setTransactionDetails(transactionDetails);
        BigDecimal totalPrice = transactionDetails.stream().map(TransactionDetails::getTotalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        transaction.setTotalPrice(totalPrice);
        return transaction;
    }

    public TransactionDto toDto(Transaction transaction) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setClientId(transaction.getClient().getId());
        transactionDto.setDate(transaction.getDate().toString());
        transactionDto.setId(transaction.getId());
        transactionDto.setTotalPrice(transaction.getTotalPrice());
        transactionDto.setTransactionDetails(transaction.getTransactionDetails().stream()
                                                     .map(transactionDetails -> transactionDetailsMapper
                                                             .toDto(transactionDetails)).collect(Collectors.toList()));
        return transactionDto;
    }
}
