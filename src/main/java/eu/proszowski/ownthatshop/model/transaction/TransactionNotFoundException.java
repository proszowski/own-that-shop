package eu.proszowski.ownthatshop.model.transaction;

import eu.proszowski.ownthatshop.model.ModelException;

public class TransactionNotFoundException extends ModelException {
    public TransactionNotFoundException() {
        super("Transaction not found");
    }
}
