package eu.proszowski.ownthatshop.model.transaction;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Collection;

@Data
public class TransactionDto {
    private Long id;
    private String date;
    private BigDecimal totalPrice;
    private Long clientId;
    private Collection<TransactionDetailsDto> transactionDetails;
}
