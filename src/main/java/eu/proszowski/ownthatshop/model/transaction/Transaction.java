package eu.proszowski.ownthatshop.model.transaction;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import eu.proszowski.ownthatshop.model.client.Client;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collection;

@Data
@Entity
@Table(name = "Transactions")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private Instant date;
    @PositiveOrZero
    @Digits(integer = 10, fraction = 2)
    private BigDecimal totalPrice;
    @OneToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;
    @JsonManagedReference
    @OneToMany(mappedBy = "transaction", cascade = CascadeType.ALL)
    private Collection<TransactionDetails> transactionDetails;
}
