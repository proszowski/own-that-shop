package eu.proszowski.ownthatshop.model.transaction;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.product.ProductFacade;
import eu.proszowski.ownthatshop.model.product.ProductNotFoundException;

import java.math.BigDecimal;

public class TransactionDetailsMapper {

    private ProductFacade productFacade;

    public TransactionDetailsMapper(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    public TransactionDetails fromDto(TransactionDetailsDto td,
            Transaction transaction) {
        TransactionDetails transactionDetails = new TransactionDetails();
        Product product = productFacade.get(td.getProductId()).orElseThrow(ProductNotFoundException::new);
        transactionDetails.setProduct(product);
        transactionDetails.setQuantity(td.getQuantity());
        transactionDetails.setTotalPrice(product.getPrice().multiply(new BigDecimal(td.getQuantity())));
        transactionDetails.setTransaction(transaction);
        return transactionDetails;
    }

    public TransactionDetailsDto toDto(TransactionDetails transactionDetails) {
        TransactionDetailsDto transactionDetailsDto = new TransactionDetailsDto();
        transactionDetailsDto.setId(transactionDetails.getId());
        transactionDetailsDto.setProductId(transactionDetails.getProduct().getId());
        transactionDetailsDto.setQuantity(transactionDetailsDto.getQuantity());
        transactionDetailsDto.setTotalPrice(transactionDetailsDto.getTotalPrice());
        transactionDetailsDto.setTransactionId(transactionDetails.getTransaction().getId());
        return transactionDetailsDto;
    }
}
