package eu.proszowski.ownthatshop.model.transaction;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository {
    Iterable<Transaction> findAll();

    Long add(Transaction transaction);

    void deleteById(Long id);

    Optional<Transaction> findById(Long id);

    Optional<List<Transaction>> findByClientId(Long id);
}
