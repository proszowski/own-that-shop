package eu.proszowski.ownthatshop.model.transaction;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionDetailsDto {
    private Long id;
    private Long productId;
    private Integer quantity;
    private BigDecimal totalPrice;
    private Long transactionId;
}
