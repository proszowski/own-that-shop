package eu.proszowski.ownthatshop.model.statistic.query.filter;

import eu.proszowski.ownthatshop.model.statistic.Range;
import eu.proszowski.ownthatshop.model.transaction.Transaction;
import lombok.var;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class DateRangeFilter extends Filter{

    private Range<Instant> range;

    public DateRangeFilter(FilterConfiguration filterConfiguration) {
        super(filterConfiguration);
        Instant start;
        Instant end;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if(filterConfiguration.getParameters().containsKey("start")){
            var tmp = LocalDate.parse(filterConfiguration.getParameters().get("start"), dateTimeFormatter);
            start = tmp.atStartOfDay().toInstant(ZoneOffset.UTC);
        }else {
            start = Instant.MIN;
        }

        if(filterConfiguration.getParameters().containsKey("end")){
            var tmp = LocalDate.parse(filterConfiguration.getParameters().get("end"), dateTimeFormatter);
            end = tmp.atStartOfDay().toInstant(ZoneOffset.UTC);
        }else {
            end = Instant.MAX;
        }

        range = new Range<>(start, end);
    }

    @Override
    public Boolean apply(Filterable filterable) {
        if(filterable.getEntity() instanceof Transaction){
            var transaction = (Transaction) filterable.getEntity();
            return range.includes(transaction.getDate());
        }

        return true;
    }
}
