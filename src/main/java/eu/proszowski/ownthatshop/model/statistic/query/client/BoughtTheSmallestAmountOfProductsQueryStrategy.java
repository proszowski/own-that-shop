package eu.proszowski.ownthatshop.model.statistic.query.client;

import eu.proszowski.ownthatshop.model.client.Client;
import eu.proszowski.ownthatshop.model.statistic.query.QueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;
import eu.proszowski.ownthatshop.model.transaction.Transaction;

import java.util.*;
import java.util.stream.Collectors;

public class BoughtTheSmallestAmountOfProductsQueryStrategy implements QueryStrategy {
    private Map<Client, Integer> clientsWithAmountOfBoughtProducts = new HashMap<>();

    @Override
    public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables) {
        otherFilterables
                .stream()
                .map(e -> (Transaction) e.getEntity())
                .map(Transaction::getTransactionDetails)
                .flatMap(Collection::stream)
                .flatMap(td -> {
                    filterables
                            .stream()
                            .map(e -> (Client) e.getEntity())
                            .forEach(client -> clientsWithAmountOfBoughtProducts
                                    .merge(client, 0, (prev, one) -> prev + td.getQuantity()));
                    return null;
                });

        return clientsWithAmountOfBoughtProducts
                .entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .map(e -> Filterable.of(e.getKey()))
                .collect(Collectors.toList());
    }
}
