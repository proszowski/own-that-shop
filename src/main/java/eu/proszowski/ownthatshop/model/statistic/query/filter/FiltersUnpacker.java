package eu.proszowski.ownthatshop.model.statistic.query.filter;

import java.util.ArrayList;
import java.util.List;

public class FiltersUnpacker {
    public static Filters unpackDto(FiltersDto filtersDto) {
        List<Filter> filters = new ArrayList<Filter>();

        if (filtersDto.getFilterConfigurations() == null) {
            return Filters.of(filters);
        }

        filtersDto.getFilterConfigurations().forEach(
                (filterName, filterConfiguration) -> {
                    switch (filterName) {
                        case COUNTRY:
                            filters.add(new CountryFilter(filterConfiguration));
                            break;
                        case CLIENT_TYPE:
                            filters.add(new ClientTypeFilter(filterConfiguration));
                            break;
                        case DATE_RANGE:
                            filters.add(new DateRangeFilter(filterConfiguration));
                            break;
                        case PRICE_RANGE:
                            filters.add(new PriceRangeFilter(filterConfiguration));
                            break;
                        case PRODUCT:
                            filters.add(new ProductFilter(filterConfiguration));
                            break;
                        case CLIENT:
                            filters.add(new ClientFilter(filterConfiguration));
                            break;
                    }
                }
        );

        return Filters.of(filters);
    }
}
