package eu.proszowski.ownthatshop.model.statistic.query.filter;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Filter {
    protected FilterConfiguration filterConfiguration;

    public abstract Boolean apply(Filterable filterable);
}
