package eu.proszowski.ownthatshop.model.statistic.query;

import eu.proszowski.ownthatshop.model.statistic.ResultType;
import eu.proszowski.ownthatshop.model.statistic.query.filter.FiltersDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class QueryDto {
    private ResultType resultType;
    private QueryName queryName;
    private FiltersDto filtersDto;
}
