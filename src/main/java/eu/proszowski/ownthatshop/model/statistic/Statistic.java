package eu.proszowski.ownthatshop.model.statistic;

import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Statistic {
    private List<Filterable> queryResult;
}
