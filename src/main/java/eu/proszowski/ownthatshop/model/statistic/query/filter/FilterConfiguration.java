package eu.proszowski.ownthatshop.model.statistic.query.filter;

import lombok.Data;

import java.util.Map;

@Data
public class FilterConfiguration {
    private Map<String, String> parameters;
}
