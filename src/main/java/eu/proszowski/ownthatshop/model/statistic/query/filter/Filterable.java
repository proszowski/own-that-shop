package eu.proszowski.ownthatshop.model.statistic.query.filter;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class Filterable {
    private Object entity;
}
