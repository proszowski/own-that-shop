package eu.proszowski.ownthatshop.model.statistic.query.client;

import eu.proszowski.ownthatshop.model.statistic.query.QueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class BoughtTheBiggestAmountOfProductsQueryStrategy implements QueryStrategy {
    @Override
    public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables) {
        return new BoughtTheSmallestAmountOfProductsQueryStrategy()
                .getQueryResult(filterables, otherFilterables)
                .stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
    }
}
