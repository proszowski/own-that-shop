package eu.proszowski.ownthatshop.model.statistic.query.filter;

import eu.proszowski.ownthatshop.model.client.Client;
import lombok.var;

public class CountryFilter extends Filter{
    private String countryName;

    public CountryFilter(FilterConfiguration filterConfiguration) {
        super(filterConfiguration);
        if(filterConfiguration.getParameters().containsKey("countryName")){
            countryName = filterConfiguration.getParameters().get("contryName");
        }else {
            countryName = "";
        }
    }


    @Override
    public Boolean apply(Filterable filterable) {
        if(filterable.getEntity() instanceof Client){
            var client = (Client) filterable.getEntity();
            return client.getAddress().getCountry().toLowerCase().equals(countryName.toLowerCase());
        }

        return true;
    }
}
