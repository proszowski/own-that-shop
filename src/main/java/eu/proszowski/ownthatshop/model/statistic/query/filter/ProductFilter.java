package eu.proszowski.ownthatshop.model.statistic.query.filter;

import eu.proszowski.ownthatshop.model.product.Product;
import lombok.var;

public class ProductFilter extends Filter {

    private Long id;

    public ProductFilter(FilterConfiguration filterConfiguration) {
        super(filterConfiguration);
        if(filterConfiguration.getParameters().containsKey("id")){
            id = Long.parseLong(filterConfiguration.getParameters().get("id"));
        }
    }

    @Override
    public Boolean apply(Filterable filterable) {
        if(id == null){
            return true;
        }

        if(filterable.getEntity() instanceof Product){
            var product = (Product) filterable.getEntity();
            return product.getId().equals(id);
        }

        return true;
    }
}
