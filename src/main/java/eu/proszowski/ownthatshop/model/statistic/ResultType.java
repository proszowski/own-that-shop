package eu.proszowski.ownthatshop.model.statistic;

public enum ResultType {
    PRODUCTS, CLIENTS, TRANSACTIONS
}
