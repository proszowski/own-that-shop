package eu.proszowski.ownthatshop.model.statistic.query;

import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;

import java.util.List;

public interface QueryStrategy {
    List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables);
}
