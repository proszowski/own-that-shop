package eu.proszowski.ownthatshop.model.statistic.query.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltersDto {
    private Map<FilterName, FilterConfiguration> filterConfigurations;
}
