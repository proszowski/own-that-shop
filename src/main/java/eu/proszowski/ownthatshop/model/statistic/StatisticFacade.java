package eu.proszowski.ownthatshop.model.statistic;

import eu.proszowski.ownthatshop.model.client.Client;
import eu.proszowski.ownthatshop.model.client.ClientFacade;
import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.product.ProductFacade;
import eu.proszowski.ownthatshop.model.statistic.query.Query;
import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;
import eu.proszowski.ownthatshop.model.statistic.query.filter.FilterableList;
import eu.proszowski.ownthatshop.model.transaction.Transaction;
import eu.proszowski.ownthatshop.model.transaction.TransactionDetails;
import eu.proszowski.ownthatshop.model.transaction.TransactionFacade;
import lombok.AllArgsConstructor;
import lombok.var;

import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
public class StatisticFacade {

    private ClientFacade clients;
    private ProductFacade products;
    private TransactionFacade transactions;

    public Statistic getStatistics(Query query, ResultType type) {
        var filters = query.getFilters();
        var filteredClients = filters.apply(new FilterableList<>(clients.getAll()).getFilterables());
        var filteredProducts = filters.apply(new FilterableList<>(products.getAll()).getFilterables());
        var filteredTransactions = filters.apply(new FilterableList<>(transactions.getAll()).getFilterables());

        var c = filteredClients.stream().map(filterable -> (Client) filterable.getEntity())
                .collect(Collectors.toList());
        var p = filteredProducts.stream().map(filterable -> (Product) filterable.getEntity())
                .collect(Collectors.toList());
        var t = filteredTransactions.stream().map(filterable -> (Transaction) filterable.getEntity())
                .collect(Collectors.toList());

        filteredTransactions = transactionsWithIncludedProductsAndClients(t, c, p);
        filteredProducts = productsWithIncludedTransactions(p, t);
        filteredClients = clientsWithIncludedTransactions(c, t);

        switch (type) {
            case PRODUCTS:
                return new Statistic(query.getQueryResult(filteredProducts, filteredTransactions));
            case CLIENTS:
                return new Statistic(query.getQueryResult(filteredClients, filteredTransactions));
            case TRANSACTIONS:
                return new Statistic(query.getQueryResult(filteredTransactions, filteredTransactions));
            default:
                return new Statistic(Collections.EMPTY_LIST);
        }
    }

    private List<Filterable> clientsWithIncludedTransactions(List<Client> c, List<Transaction> t) {
        return c
                .stream()
                .filter(client -> t
                        .stream()
                        .map(transaction -> transaction.getClient().getId())
                        .collect(Collectors.toList())
                        .contains(client.getId())
                )
                .map(Filterable::of)
                .collect(Collectors.toList());
    }

    private List<Filterable> productsWithIncludedTransactions(List<Product> p, List<Transaction> t) {
        return p
                .stream()
                .filter(product -> t
                        .stream()
                        .map(Transaction::getTransactionDetails)
                        .flatMap(Collection::stream)
                        .map(td -> td.getProduct().getId())
                        .collect(Collectors.toList())
                        .contains(product.getId())
                )
                .map(Filterable::of)
                .collect(Collectors.toList());
    }

    private List<Filterable> transactionsWithIncludedProductsAndClients(List<Transaction> t, List<Client> c,
            List<Product> p) {

        return t
                .stream()
                .filter(transaction -> c
                        .stream()
                        .map(Client::getId)
                        .collect(Collectors.toList())
                        .contains(transaction.getClient().getId())
                )
                .filter(transaction -> transaction
                        .getTransactionDetails()
                        .stream()
                        .filter(transactionDetails -> p
                                .stream()
                                .map(Product::getId)
                                .collect(Collectors.toList())
                                .contains(transactionDetails.getProduct().getId()))
                        .map(td -> {
                            Map<TransactionDetails, Product> m = new HashMap<>();
                            m.put(td, td.getProduct());
                            return m;
                        })
                        .map(Map::keySet)
                        .flatMap(Collection::stream)
                        .map(transactionDetails -> transactionDetails.getTransaction().getId())
                        .collect(Collectors.toList())
                        .contains(transaction.getId())
                )
                .map(Filterable::of)
                .collect(Collectors.toList());
    }
}
