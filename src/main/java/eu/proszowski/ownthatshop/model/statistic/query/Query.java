package eu.proszowski.ownthatshop.model.statistic.query;

import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;
import eu.proszowski.ownthatshop.model.statistic.query.filter.Filters;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.logging.Filter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Query {
    private Filters filters;
    private QueryStrategy queryStrategy;

    public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> transactions){
        if(queryStrategy != null){
            return queryStrategy.getQueryResult(filterables, transactions);
        }

        return filterables;
    }
}
