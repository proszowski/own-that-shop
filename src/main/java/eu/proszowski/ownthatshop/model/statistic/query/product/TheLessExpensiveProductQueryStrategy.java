package eu.proszowski.ownthatshop.model.statistic.query.product;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.statistic.query.QueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TheLessExpensiveProductQueryStrategy implements QueryStrategy {
    @Override
    public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables) {
        return filterables
                .stream()
                .map(e -> (Product) e.getEntity())
                .sorted(Comparator.comparing(Product::getPrice))
                .map(Filterable::of)
                .collect(Collectors.toList());
    }
}
