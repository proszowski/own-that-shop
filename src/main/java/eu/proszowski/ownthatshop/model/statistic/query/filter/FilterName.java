package eu.proszowski.ownthatshop.model.statistic.query.filter;

public enum FilterName {
    COUNTRY,
    CLIENT_TYPE,
    DATE_RANGE,
    PRICE_RANGE,
    PRODUCT,
    CLIENT
}
