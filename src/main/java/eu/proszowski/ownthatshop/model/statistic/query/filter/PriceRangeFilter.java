package eu.proszowski.ownthatshop.model.statistic.query.filter;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.statistic.Range;
import lombok.var;

import java.math.BigDecimal;
import java.util.Optional;

public class PriceRangeFilter extends Filter {

    private Range<BigDecimal> priceRange;

    public PriceRangeFilter(FilterConfiguration filterConfiguration) {
        super(filterConfiguration);
        BigDecimal start = new BigDecimal(Integer.MIN_VALUE);
        BigDecimal end = new BigDecimal(Integer.MAX_VALUE);

        if (filterConfiguration.getParameters().containsKey("start")) {
            start = new BigDecimal(filterConfiguration.getParameters().get("start"));
        }

        if (filterConfiguration.getParameters().containsKey("end")) {
            end = new BigDecimal(filterConfiguration.getParameters().get("start"));
        }

        priceRange = new Range<>(start, end);

    }

    @Override
    public Boolean apply(Filterable filterable) {
        if (filterable.getEntity() instanceof Product) {
            var product = (Product) filterable.getEntity();
            return priceRange.includes(Optional.of(product.getPrice()).orElse(BigDecimal.ZERO));
        }
        return true;
    }
}
