package eu.proszowski.ownthatshop.model.statistic.query.product;

import eu.proszowski.ownthatshop.model.statistic.query.QueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TheMostPopularProductQueryStrategy implements QueryStrategy {

    @Override
    public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables) {
        List<Filterable> queryResult = new TheLessPopularProductQueryStrategy()
                .getQueryResult(filterables, otherFilterables);
        return queryResult
                .stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
    }
}
