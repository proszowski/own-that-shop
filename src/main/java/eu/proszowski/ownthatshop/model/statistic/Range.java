package eu.proszowski.ownthatshop.model.statistic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Range<T extends Comparable> {

    private T start;
    private T end;

    public Boolean includes(T check){
        return check.compareTo(start) >= 0 && check.compareTo(end) <= 0;
    }
}
