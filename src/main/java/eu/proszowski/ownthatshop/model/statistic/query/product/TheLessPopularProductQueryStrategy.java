package eu.proszowski.ownthatshop.model.statistic.query.product;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.statistic.query.QueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.filter.Filterable;
import eu.proszowski.ownthatshop.model.transaction.Transaction;

import java.util.*;
import java.util.stream.Collectors;

public class TheLessPopularProductQueryStrategy implements QueryStrategy {
    @Override
    public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables) {
        Map<Product, Integer> productAndQuantityFromTransactionDetails = new HashMap<>();
        otherFilterables
                .stream()
                .map(e -> (Transaction) e.getEntity())
                .map(Transaction::getTransactionDetails)
                .flatMap(Collection::stream)
                .flatMap(
                        transactionDetails -> {
                            filterables
                                    .stream()
                                    .map(e -> (Product) e.getEntity())
                                    .filter(product -> transactionDetails.getProduct().getId().equals(product.getId()))
                                    .forEach(product -> productAndQuantityFromTransactionDetails
                                            .merge(product, 0, (prev, one) -> prev + transactionDetails.getQuantity()));
                            return null;
                        }
                );

        return productAndQuantityFromTransactionDetails.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .map(e -> Filterable.of(e.getKey()))
                .collect(Collectors.toList());
    }
}
