package eu.proszowski.ownthatshop.model.statistic.query.filter;

import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FilterableList<T> {
    @Getter
    private List<Filterable> filterables;

    public FilterableList(Collection<T> objects) {
        filterables = new ArrayList<Filterable>();
        objects.forEach(o -> filterables.add(Filterable.of(o)));
    }
}
