package eu.proszowski.ownthatshop.model.statistic.query.filter;

import eu.proszowski.ownthatshop.model.client.Client;
import lombok.var;

public class ClientFilter extends Filter {

    private Long id;

    public ClientFilter(FilterConfiguration filterConfiguration) {
        super(filterConfiguration);
        if (filterConfiguration.getParameters().containsKey("id")) {
            id = Long.parseLong(filterConfiguration.getParameters().get("id"));
        }
    }

    @Override
    public Boolean apply(Filterable filterable) {
        if (id == null) {
            return true;
        }

        if (filterable.getEntity() instanceof Client) {
            var client = (Client) filterable.getEntity();
            return client.getId().equals(id);
        }

        return true;
    }
}
