package eu.proszowski.ownthatshop.model.statistic.query;

import eu.proszowski.ownthatshop.model.statistic.query.client.BoughtTheBiggestAmountOfProductsQueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.client.BoughtTheSmallestAmountOfProductsQueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.filter.FiltersUnpacker;
import eu.proszowski.ownthatshop.model.statistic.query.product.TheLessExpensiveProductQueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.product.TheLessPopularProductQueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.product.TheMostExpensiveProductsQueryStrategy;
import eu.proszowski.ownthatshop.model.statistic.query.product.TheMostPopularProductQueryStrategy;
import lombok.var;

public class QueryBinder {
    public static Query bind(QueryDto queryDto){
        var filters = FiltersUnpacker.unpackDto(queryDto.getFiltersDto());
        return new Query(filters, getQueryStrategy(queryDto.getQueryName()));
    }

    private static QueryStrategy getQueryStrategy(QueryName queryDtoName){
        if(queryDtoName == null){
            return null;
        }

        switch (queryDtoName){
            case THE_MOST_EXPENSIVE_PRODUCT:
                return new TheMostExpensiveProductsQueryStrategy();
            case THE_LESS_EXPENSIVE_PRODUCT:
                return new TheLessExpensiveProductQueryStrategy();
            case THE_MOST_POPULAR_PRODUCT:
                return new TheMostPopularProductQueryStrategy();
            case THE_LESS_POPULAR_PRODUCT:
                return new TheLessPopularProductQueryStrategy();
            case BOUGHT_THE_BIGGEST_AMOUNT_OF_PRODUCTS:
                return new BoughtTheBiggestAmountOfProductsQueryStrategy();
            case BOUGHT_THE_SMALLEST_AMOUNT_OF_PRODUCTS:
                return new BoughtTheSmallestAmountOfProductsQueryStrategy();
            default:
                throw new IllegalArgumentException();
        }

    }
}
