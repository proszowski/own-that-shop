package eu.proszowski.ownthatshop.model.statistic.query.filter;

import eu.proszowski.ownthatshop.model.client.Client;
import lombok.var;

public class ClientTypeFilter extends Filter{

    private String clientTypeName;

    public ClientTypeFilter(FilterConfiguration filterConfiguration) {
        super(filterConfiguration);
        if(filterConfiguration.getParameters().containsKey("name")){
            clientTypeName = filterConfiguration.getParameters().get("name");
        }else {
            clientTypeName = "";
        }
    }

    @Override
    public Boolean apply(Filterable filterable) {
        if(filterable.getEntity() instanceof Client){
            var client = (Client) filterable.getEntity();
            return client.getClientType().getName().toLowerCase().equals(clientTypeName.toLowerCase());
        }
        return true;
    }
}
