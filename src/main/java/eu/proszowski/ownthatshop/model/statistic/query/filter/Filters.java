package eu.proszowski.ownthatshop.model.statistic.query.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.var;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
public class Filters {
    private List<Filter> filters = new ArrayList<>();

    public List<Filterable> apply(List<Filterable> filterables){
        var filteredList = new ArrayList<Filterable>(filterables);
        filterables.forEach(
                filterable -> filters.forEach(
                        filter -> {
                            if(!filter.apply(filterable)){
                                filteredList.remove(filterable);
                            }
                        }
                )
        );

        return filteredList;
    }
}
