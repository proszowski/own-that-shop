package eu.proszowski.ownthatshop.model.product;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product_types")
public @Data
class ProductType {
    @Id
    private Long id;
    private String name;
}
