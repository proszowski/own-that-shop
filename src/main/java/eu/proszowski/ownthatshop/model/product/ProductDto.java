package eu.proszowski.ownthatshop.model.product;

import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Long Id;
    private String name;
    private BigDecimal price;
    private Integer quantity;
    private String image;
    private Long productTypeId;

}
