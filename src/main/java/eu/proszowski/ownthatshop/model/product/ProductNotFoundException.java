package eu.proszowski.ownthatshop.model.product;

import eu.proszowski.ownthatshop.model.ModelException;

public class ProductNotFoundException extends ModelException {

    public ProductNotFoundException() {
        super("product not found");
    }
}
