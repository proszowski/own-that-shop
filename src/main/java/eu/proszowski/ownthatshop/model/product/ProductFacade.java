package eu.proszowski.ownthatshop.model.product;

import eu.proszowski.ownthatshop.database.product.ProductRepository;
import eu.proszowski.ownthatshop.model.ModelException;
import eu.proszowski.ownthatshop.model.transaction.TransactionFacade;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ProductFacade {

    private ProductRepository productRepository;

    public ProductFacade(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Collection<Product> getAll() {
        List<Product> products = new LinkedList<>();
        productRepository.findAll().forEach(products::add);
        return products;
    }

    public Long add(Product product) {
        product.setId(null);
        try {
            return productRepository.save(product);
        } catch (Exception e) {
            throw new ModelException("product couldn't be saved.");
        }
    }

    public Optional<ProductType> findTypeById(Long productTypeId) {
        return productRepository.findTypeById(productTypeId);
    }

    public void update(Product product) {
        if (product.getId() == null) {
            throw new ProductNotFoundException();
        }

        Optional<Product> existingProduct = productRepository.findProductById(product.getId());

        if (existingProduct.isPresent()) {
            productRepository.update(product);
        } else {
            throw new ProductNotFoundException();
        }
    }

    public Optional<Product> get(Long id) {
        return productRepository.findProductById(id);
    }

    public void remove(Long id) {
        try{
            productRepository.remove(id);
        }catch (Exception e){
            throw new ProductNotFoundException();
        }
    }

    public Collection<ProductType> getAvailableTypes() {
        List<ProductType> productTypes = new LinkedList<>();
        productRepository.findAllProductTypes().forEach(productTypes::add);
        return productTypes;
    }
}
