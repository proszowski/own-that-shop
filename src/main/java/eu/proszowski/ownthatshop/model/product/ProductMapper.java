package eu.proszowski.ownthatshop.model.product;

import java.util.Optional;

public class ProductMapper {

    private ProductFacade productFacade;

    public ProductMapper(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    public Product toProduct(ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        product.setQuantity(productDto.getQuantity());
        product.setImage(productDto.getImage());
        if(productDto.getProductTypeId() == null){
            throw new ProductNotFoundException();
        }
        Optional<ProductType> productType = productFacade.findTypeById(productDto.getProductTypeId());
        productType.ifPresent(product::setProductType);
        return product;
    }

    public ProductDto toDto(Product product) {
        return new ProductDto(
                product.getId(),
                product.getName(),
                product.getPrice(),
                product.getQuantity(),
                product.getImage(),
                product.getProductType().getId()
        );
    }
}
