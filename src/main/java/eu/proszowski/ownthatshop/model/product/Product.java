package eu.proszowski.ownthatshop.model.product;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Entity
@Table(name = "products")
@Data
@Validated
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank
    @Length(min = 5)
    private String name;
    @Positive
    @Digits(integer = 10, fraction = 2)
    private BigDecimal price;
    @PositiveOrZero
    private Integer quantity;
    private String image;
    @OneToOne
    @JoinColumn(name = "product_type_id", nullable = false)
    private ProductType productType;

}
