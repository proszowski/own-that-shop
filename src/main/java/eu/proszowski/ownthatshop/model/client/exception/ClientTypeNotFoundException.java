package eu.proszowski.ownthatshop.model.client.exception;

import eu.proszowski.ownthatshop.model.ModelException;

public class ClientTypeNotFoundException extends ModelException {
    public ClientTypeNotFoundException() {
        super("Client type not found");
    }
}
