package eu.proszowski.ownthatshop.model.client.exception;

import eu.proszowski.ownthatshop.model.ModelException;

public class ClientNotFoundException extends ModelException {
    public ClientNotFoundException() {
        super("Client not found");
    }
}
