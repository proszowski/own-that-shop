package eu.proszowski.ownthatshop.model.client;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "addresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @NotBlank
    @Length(max = 50)
    private String street;
    @NotNull
    @NotBlank
    @Length(max = 30)
    private String city;
    @NotNull
    @NotBlank
    @Length(max = 30)
    private String country;
    @Digits(integer = 5, fraction = 0)
    private Integer houseNumber;
}
