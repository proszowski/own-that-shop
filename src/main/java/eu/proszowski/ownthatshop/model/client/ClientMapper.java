package eu.proszowski.ownthatshop.model.client;

import eu.proszowski.ownthatshop.model.client.exception.ClientTypeNotFoundException;

import java.util.Optional;

public class ClientMapper {

    private ClientFacade clientFacade;

    public ClientMapper(ClientFacade clientFacade) {
        this.clientFacade = clientFacade;
    }

    public Client toClient(ClientDto clientDto) {
        Client client = new Client();
        client.setAddress(clientDto.getAddress());
        client.setId(clientDto.getId());
        if(clientDto.getClientTypeId() == null){
            throw new ClientTypeNotFoundException();
        }
        Optional<ClientType> clientType = clientFacade.getClientTypeById(clientDto.getClientTypeId());
        if (clientType.isPresent()) {
            client.setClientType(clientType.get());
        } else {
            throw new ClientTypeNotFoundException();
        }
        client.setCompany(clientDto.getCompany());
        client.setFirstName(clientDto.getFirstName());
        client.setSurname(clientDto.getSurname());
        client.setContact(clientDto.getContact());
        return client;
    }

    public ClientDto toDto(Client client){
        return new ClientDto(
                client.getId(),
                client.getFirstName(),
                client.getSurname(),
                client.getClientType().getId(),
                client.getCompany(),
                client.getContact(),
                client.getAddress()
        );
    }
}
