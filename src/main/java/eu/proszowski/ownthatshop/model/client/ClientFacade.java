package eu.proszowski.ownthatshop.model.client;

import eu.proszowski.ownthatshop.database.client.ClientRepository;
import eu.proszowski.ownthatshop.model.ModelException;
import eu.proszowski.ownthatshop.model.client.exception.ClientNotFoundException;
import eu.proszowski.ownthatshop.model.client.exception.ClientTypeNotFoundException;
import lombok.val;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public class ClientFacade {

    private ClientRepository clientRepository;

    public ClientFacade(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Collection<Client> getAll() {
        return clientRepository.findAll();
    }

    public Client getById(Long id) {
        Optional<Client> optionalClient = clientRepository.findById(id);
        if (optionalClient.isPresent()) {
            return optionalClient.get();
        } else {
            throw new ClientNotFoundException();
        }
    }

    public Long add(Client client) {
        client.setId(null);
        Long clientTypeId = client.getClientType().getId();
        Optional<ClientType> clientType = clientRepository.getClientTypeById(clientTypeId);
        if (clientType.isPresent()) {
            client.setClientType(clientType.get());
        } else {
            throw new ClientTypeNotFoundException();
        }
        try {
            return clientRepository.add(client);
        } catch (Exception e) {
            throw new ModelException("Client couldn't be saved");
        }
    }

    public Optional<ClientType> getClientTypeById(Long clientTypeId) {
        return clientRepository.getClientTypeById(clientTypeId);
    }

    public void remove(Long id) {
        this.clientRepository.remove(id);
    }

    public void update(Client client) {
        clientRepository.update(client);
    }

    public Collection<ClientType> getAllClientTypes() {
        val clientTypes = new ArrayList<ClientType>();
        clientRepository.getAllClientTypes().forEach(clientTypes::add);
        return clientTypes;
    }

    public Collection<Company> getAllCompanies() {
        val companies = new ArrayList<Company>();
        clientRepository.getAllCompanies().forEach(companies::add);
        return companies;
    }

    public Client getByEmail(String email) {
        Optional<Client> client = clientRepository.findByEmail(email);
        if(client.isPresent()){
            return client.get();
        }else {
            throw new ClientNotFoundException();
        }
    }
}
