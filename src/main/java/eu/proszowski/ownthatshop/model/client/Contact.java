package eu.proszowski.ownthatshop.model.client;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @Length(min = 9, max = 9)
    private String phoneNumber;
    @Email
    @NotNull
    private String email;
}
