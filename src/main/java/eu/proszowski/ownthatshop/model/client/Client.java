package eu.proszowski.ownthatshop.model.client;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name = "clients")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank
    @Length(max = 20)
    private String firstName;
    @NotBlank
    @Length(max = 30)
    private String surname;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    private Company company;
    @OneToOne
    @Valid
    private ClientType clientType;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    private Contact contact;
    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    private Address address;
}
