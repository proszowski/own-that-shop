package eu.proszowski.ownthatshop.model.client;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientDto {
    private Long id;
    private String firstName;
    private String surname;
    private Long clientTypeId;
    private Company company;
    private Contact contact;
    private Address address;
}
