package eu.proszowski.ownthatshop.database.transaction;

import eu.proszowski.ownthatshop.model.transaction.Transaction;
import eu.proszowski.ownthatshop.model.transaction.TransactionRepository;

import java.util.List;
import java.util.Optional;

public class TransactionDatabase implements TransactionRepository {

    private JpaTransactionRepository jpaTransactionRepository;

    public TransactionDatabase(JpaTransactionRepository jpaTransactionRepository) {
        this.jpaTransactionRepository = jpaTransactionRepository;
    }

    @Override
    public Iterable<Transaction> findAll() {
        return jpaTransactionRepository.findAll();
    }

    @Override
    public Long add(Transaction transaction) {
        Transaction savedTransaction = jpaTransactionRepository.save(transaction);
        return savedTransaction.getId();
    }

    @Override
    public void deleteById(Long id) {
        jpaTransactionRepository.deleteById(id);
    }

    @Override
    public Optional<Transaction> findById(Long id) {
        return jpaTransactionRepository.findById(id);
    }

    @Override
    public Optional<List<Transaction>> findByClientId(Long id) {
        return jpaTransactionRepository.findByClientId(id);
    }
}
