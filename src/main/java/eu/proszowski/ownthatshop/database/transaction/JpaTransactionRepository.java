package eu.proszowski.ownthatshop.database.transaction;

import eu.proszowski.ownthatshop.model.transaction.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface JpaTransactionRepository extends CrudRepository<Transaction, Long> {
    Optional<List<Transaction>> findByClientId(Long id);
}
