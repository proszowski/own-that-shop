package eu.proszowski.ownthatshop.database.client;

import eu.proszowski.ownthatshop.model.client.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface JpaClientRepository extends CrudRepository<Client, Long> {
    Optional<Client> findByContactEmail(String email);
}
