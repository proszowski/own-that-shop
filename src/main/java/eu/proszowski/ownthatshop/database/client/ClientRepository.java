package eu.proszowski.ownthatshop.database.client;

import eu.proszowski.ownthatshop.model.client.Client;
import eu.proszowski.ownthatshop.model.client.ClientType;
import eu.proszowski.ownthatshop.model.client.Company;

import java.util.Collection;
import java.util.Optional;

public interface ClientRepository {
    Collection<Client> findAll();

    Optional<Client> findById(Long id);

    Optional<ClientType> getClientTypeById(Long clientTypeId);

    Long add(Client client);

    void remove(Long id);

    void update(Client client);

    Iterable<ClientType> getAllClientTypes();

    Iterable<Company> getAllCompanies();

    Optional<Client> findByEmail(String email);
}
