package eu.proszowski.ownthatshop.database.client;

import eu.proszowski.ownthatshop.model.client.Address;
import org.springframework.data.repository.CrudRepository;

public interface JpaAddressRepository extends CrudRepository<Address, Long> {
}
