package eu.proszowski.ownthatshop.database.client;

import eu.proszowski.ownthatshop.model.client.Company;
import org.springframework.data.repository.CrudRepository;

public interface JpaCompanyRepository extends CrudRepository<Company, Long> {
}
