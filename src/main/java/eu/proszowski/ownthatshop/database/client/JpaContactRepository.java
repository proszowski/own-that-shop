package eu.proszowski.ownthatshop.database.client;

import eu.proszowski.ownthatshop.model.client.Contact;
import org.springframework.data.repository.CrudRepository;

public interface JpaContactRepository extends CrudRepository<Contact, Long> {
}
