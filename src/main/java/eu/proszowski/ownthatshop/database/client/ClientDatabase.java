package eu.proszowski.ownthatshop.database.client;

import eu.proszowski.ownthatshop.model.client.Client;
import eu.proszowski.ownthatshop.model.client.ClientType;
import eu.proszowski.ownthatshop.model.client.Company;
import eu.proszowski.ownthatshop.model.client.exception.ClientNotFoundException;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

public class ClientDatabase implements ClientRepository{

    private JpaClientRepository jpaClientRepository;
    private JpaClientTypeRepository jpaClientTypeRepository;
    private JpaContactRepository jpaContactRepository;
    private JpaAddressRepository jpaAddressRepository;
    private JpaCompanyRepository jpaCompanyRepository;

    public ClientDatabase(JpaClientRepository jpaClientRepository, JpaClientTypeRepository jpaClientTypeRepository, JpaContactRepository jpaContactRepository, JpaAddressRepository jpaAddressRepository, JpaCompanyRepository jpaCompanyRepository) {
        this.jpaClientRepository = jpaClientRepository;
        this.jpaClientTypeRepository = jpaClientTypeRepository;
        this.jpaContactRepository = jpaContactRepository;
        this.jpaAddressRepository = jpaAddressRepository;
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    @Override
    public Collection<Client> findAll() {
        Collection<Client> clients = new LinkedList<>();
        jpaClientRepository.findAll().forEach(clients::add);
        return clients;
    }

    @Override
    public Optional<Client> findById(Long id) {
        return jpaClientRepository.findById(id);
    }

    @Override
    public Optional<ClientType> getClientTypeById(Long clientTypeId) {
        return jpaClientTypeRepository.findById(clientTypeId);
    }

    @Override
    @Transactional
    public Long add(Client client) {
        client.getContact().setId(null);
        client.getAddress().setId(null);
        client.getCompany().setId(null);
        jpaContactRepository.save(client.getContact());
        jpaAddressRepository.save(client.getAddress());
        jpaCompanyRepository.save(client.getCompany());
        Client responseClient = jpaClientRepository.save(client);
        return responseClient.getId();
    }

    @Override
    public void remove(Long id) {
        jpaClientRepository.deleteById(id);
    }

    @Override
    public void update(Client client) {
        Long id = client.getId();
        if(id == null){
            throw new ClientNotFoundException();
        }

        Optional<Client> optionalClient = jpaClientRepository.findById(id);
        if(optionalClient.isPresent()){
            jpaClientRepository.save(client);
        } else {
            throw new ClientNotFoundException();
        }
    }

    @Override
    public Iterable<ClientType> getAllClientTypes() {
        return jpaClientTypeRepository.findAll();
    }

    @Override
    public Iterable<Company> getAllCompanies() {
        return jpaCompanyRepository.findAll();
    }

    @Override
    public Optional<Client> findByEmail(String email) {
        return jpaClientRepository.findByContactEmail(email);
    }
}
