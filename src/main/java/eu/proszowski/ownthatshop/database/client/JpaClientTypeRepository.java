package eu.proszowski.ownthatshop.database.client;

import eu.proszowski.ownthatshop.model.client.ClientType;
import org.springframework.data.repository.CrudRepository;

public interface JpaClientTypeRepository extends CrudRepository<ClientType, Long> {
}
