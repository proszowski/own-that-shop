package eu.proszowski.ownthatshop.database.product;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.product.ProductType;

import java.util.Optional;

public interface ProductRepository {
    Optional<ProductType> findTypeById(Long productTypeId);

    Iterable<Product> findAll();

    Long save(Product product);

    Optional<Product> findProductById(Long id);

    void update(Product product);

    void remove(Long id);

    Iterable<ProductType> findAllProductTypes();
}
