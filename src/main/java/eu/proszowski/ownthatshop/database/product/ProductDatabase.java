package eu.proszowski.ownthatshop.database.product;

import eu.proszowski.ownthatshop.model.product.Product;
import eu.proszowski.ownthatshop.model.product.ProductType;

import java.util.Optional;

public class ProductDatabase implements ProductRepository{

    private JpaProductRepository productRepository;
    private JpaProductTypeRepository productTypeRepository;

    public ProductDatabase(JpaProductRepository productRepository, JpaProductTypeRepository productTypeRepository) {
        this.productRepository = productRepository;
        this.productTypeRepository = productTypeRepository;
    }

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public Long save(Product product) {
        return productRepository.save(product).getId();
    }

    @Override
    public Optional<Product> findProductById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public void update(Product product) {
        productRepository.save(product);
    }

    @Override
    public void remove(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Iterable<ProductType> findAllProductTypes() {
        return productTypeRepository.findAll();
    }

    @Override
    public Optional<ProductType> findTypeById(Long productTypeId) {
        return productTypeRepository.findById(productTypeId);
    }
}
