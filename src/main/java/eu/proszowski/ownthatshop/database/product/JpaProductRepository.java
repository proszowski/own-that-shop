package eu.proszowski.ownthatshop.database.product;

import eu.proszowski.ownthatshop.model.product.Product;
import org.springframework.data.repository.CrudRepository;

public interface JpaProductRepository extends CrudRepository<Product, Long> {
}
