package eu.proszowski.ownthatshop.database.product;

import eu.proszowski.ownthatshop.model.product.ProductType;
import org.springframework.data.repository.CrudRepository;

public interface JpaProductTypeRepository extends CrudRepository<ProductType, Long> {
}
