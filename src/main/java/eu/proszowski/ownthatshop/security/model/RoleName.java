package eu.proszowski.ownthatshop.security.model;

public enum RoleName {
    ROLE_USER,
    ROLE_GUEST,
    ROLE_ADMIN
}
