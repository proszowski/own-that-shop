package eu.proszowski.ownthatshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class OwnThatShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(OwnThatShopApplication.class, args);
    }

}

